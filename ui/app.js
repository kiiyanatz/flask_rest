Vue.component('task', {
    template: '<li>Test component</li>'
});

new Vue({
    el: '#app',

    data: {
        title: 'Vue-Todo',
        task_title: '',
        task_description: '',
        message: 'Get productive!',
        tasks: [],
        btnTitle: 'Add new task.',
        className: ''
    },

    mounted() {
        axios.get('http://localhost:5000/tasks').then(
            response => this.tasks = response.data
        );
    },

    methods: {
        deleteTask: function(task) {
            var task_array = this.tasks
            task_index = task_array.indexOf(task);
            if (task_index > -1) {
                task_array.splice(task_index, 1);
            }
            axios.delete('http://localhost:5000/task/' + task.id).then(
                response => this.message = response.data.message
            );
        },
        addTask: function() {
            this.tasks.push({
                'title': this.task_title,
                'description': this.task_description,
                'done': false
            });
            this.saveTask();
            this.task_description = '';
            this.task_title = '';
        },
        checkDisabled: function() {
            this.isDisabled = true;
        },
        saveTask: function() {
            axios.put('http://localhost:5000/tasks', {
                title: this.task_title,
                description: this.task_description
            }).then(
                response => this.message = response.data.message
            )
        },
        completeTask: function(task) {
            task.done = true;
            this.className = 'not-active';
            axios.patch('http://localhost:5000/task/' + task.id, {
                done: true
            }).then(
                response => this.message = response.data.message
            )
            this.message = 'Task \"' + task.title + '\" has been completed!';
        }
    },

    computed: {
        numOfTasks() {
            return this.tasks.length
        },

        numOfInCompleteTasks() {
            incomplete = this.tasks.filter(task => !task.done).length
            return incomplete;
        },

        incompleteTasks() {
            return this.tasks.filter(task => !task.done)
        },

        completedTasks() {
            return this.tasks.filter(task => task.done)
        },

        numOfcompleteTasks() {
            completed = this.tasks.filter(task => task.done)
            return completed.length;
        }
    }
});
