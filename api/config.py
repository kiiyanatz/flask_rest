"""Application configuration file."""
import os

base_dir = os.path.abspath(os.path.dirname(__file__))
print base_dir


class DevConfig:
    """Configuration for development environment."""

    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + base_dir + '/app.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    DEBUG = True
