"""Application initialization file."""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_restful import Api
from flask_cors import CORS, cross_origin


app = Flask(__name__)
app.config.from_object('config.DevConfig')
CORS(app)

api = Api(app)
db = SQLAlchemy(app)
ma = Marshmallow(app)

from app import models, routes
