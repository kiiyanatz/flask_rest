"""Api endpoints and routes."""
from flask import jsonify, make_response
from flask_restful import Resource, reqparse

from app import api, db
from app.models import Task, task_schema

parser = reqparse.RequestParser()
parser.add_argument('title', type=str, help='Title of the task')
parser.add_argument('description', type=str, help='Task description')


class Index(Resource):
    """Default landing api class."""

    def get(self):
        """Landing route."""
        return {'hello': 'world'}


class TasksList(Resource):
    """List all tasks."""

    def get(self):
        """List tasks."""
        if len(Task.query.all()) == 0:
            return jsonify({"message": "No tasks available"})
        tasks = task_schema.dump(Task.query.all(), many=True).data
        resp = make_response(jsonify(tasks))
        return resp

    def put(self):
        """Add new task."""
        args = parser.parse_args()
        task = Task(**args)
        db.session.add(task)
        db.session.commit()
        resp = make_response(jsonify({"message": 'Task "' + task.title + '" has been added!'}))
        return resp


class TaskItem(Resource):
    """Perform task crud operations."""

    def get(self, task_id):
        """Get single todo task."""
        return task_schema.jsonify(Task.query.get(task_id))

    def delete(self, task_id):
        """Delete todo task."""
        db.session.delete(Task.query.get(task_id))
        db.session.commit()
        resp = make_response(jsonify({'message': 'Task has been deleted'}))
        return resp

    def put(self, task_id):
        """Edit task."""
        args = parser.parse_args()
        task = Task.query.filter_by(id=task_id).first()
        task.title = args['title']
        task.description = args['description']
        db.session.commit()
        return jsonify(task_schema.dump(task).data)

    def patch(self, task_id):
        """Update task."""
        args = parser.parse_args()
        task = Task.query.filter_by(id=task_id).first()
        print task.title
        task.done = args['done']
        db.session.commit()
        resp = make_response(jsonify({"message": "Task has been completed"}))
        return resp


api.add_resource(Index, '/')
api.add_resource(TasksList, '/tasks')
api.add_resource(TaskItem, '/task/<int:task_id>')
