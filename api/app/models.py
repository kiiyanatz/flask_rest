"""Application database models."""
from app import db, ma


class Task(db.Model):
    """Defines the application task model."""

    __tablename__ = 'tasks'
    id = db.Column(db.Integer(), primary_key=True, unique=True)
    title = db.Column(db.String(200))
    description = db.Column(db.String())
    done = db.Column(db.Boolean, default=False, nullable=False)

    def __init__(self, title, description):
        """Initialize model."""
        self.title = title
        self.description = description

    def __repr__(self):
        """Model representation."""
        return '<Task %r>' % self.title


class TaskSchema(ma.ModelSchema):
    class Meta:
        # Fields to expose
        fields = ('id', 'title', 'description', 'done')


task_schema = TaskSchema()
