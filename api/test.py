"""Unittests for api."""
from app import app
import unittest


class FlaskTestCase(unittest.TestCase):
    """Api test cases."""

    def test_index(self):
        """Test api default endpoint."""
        tester = app.test_client(self)
        response = tester.get('/', content_type='application/json')
        self.assertEqual(response.status_code, 200)

    def test_get_tasks(self):
        """Test listing all tasks."""
        tester = app.test_client(self)
        response = tester.get('/tasks', content_type='application/json')
        self.assertEqual(response.status_code, 200)

    def test_delete_task(self):
        """Test deleting task."""
        tester = app.test_client(self)
        response = tester.delete('/task/7', content_type='application/json')
        self.assertEqual(b'Task has been deleted', response.data.message)

if __name__ == '__main__':
    unittest.main()
